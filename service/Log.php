<?php
namespace Service;

class Log {
    protected $name;
    protected $path;
    protected $logfile;
    public function __construct($folder)
    {
        $this -> path = $folder;
        $this -> name = date('d_m_Y') . '.txt';
        $this -> open();
    }
    public function open() {
        $this -> logfile = fopen($this -> path . '/'. $this -> name, 'a+');
    }
    public function write($message) {
        $msg = date('H:i:s') . ' - '. $message . "\r\n";
        fwrite($this -> logfile, $msg);
    }

    public function __destruct()
    {
        fclose($this -> logfile);
    }
}