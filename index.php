<?php
define ('LOG_FOLDER', __DIR__ . '/logs');

require __DIR__ . '/vendor/autoload.php';

use App\Sequence;

$m = 5;
$n = 20;
$digits = function(int $length)
{
    while ($length > 0) {
        yield mt_rand();
        --$length;
    }
};
$sequence = new Sequence($m);
foreach ($digits($n) as $number) {
   $sequence->add($number);
}
print_r($sequence->getMaxNumbers());