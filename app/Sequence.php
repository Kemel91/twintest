<?php
namespace App;

use Service\Log;

class Sequence {
    public $numbers;
    private $length;
    private $log;

    public function __construct(int $length)
    {
        $this -> length = $length;
        $this -> log = new Log(LOG_FOLDER);
    }

    /**
     * Add numbers to array
     *
     * @param integer $number
     * @return void
     */
    public function add(int $number) {
        $this -> numbers[] = $number;
        $this -> log -> write('Add number '. $number . ' to array');
    }

    /**
     * Return numbers
     *
     * @return array
     */
    public function getMaxNumbers() {
        $this -> numberSort();
        $numbers = $this -> numberSlice();
        $this -> log -> write('Responce numbers');
        return $numbers;
    }

    /**
     * Sort numbers
     *
     * @return void
     */
    private function numberSort() {
        $this -> log -> write('Sort numbers');
        arsort($this -> numbers, SORT_NUMERIC);
    }

    /**
     * Return n- numbers
     *
     * @return array
     */
    private function numberSlice() {
        $this -> log -> write('Slice array');
        return array_slice($this -> numbers, 0, $this -> length);
    }
}